from flask import request, Response
from flask_jwt_extended import jwt_required, decode_token
from flask_restful import Resource
from database.folder import Folder
from database.document import Document
import json

class FoldersApi(Resource):
    @jwt_required
    def get(self):
        folders = Folder.objects().to_json()
        return {
            "error": False,
            "data": json.loads(folders)
        }, 200

class FolderApi(Resource):

    @jwt_required
    def post(self):
        try:
            token = request.headers.get('Authorization').split()[1]
            identity = decode_token(token)
            body = request.get_json()
            body.update({
                "type_":"folder", 
                "owner_id": identity["user_id"],
                "company_id": identity["company_id"]
            })
            folder = Folder.objects(id=body["id"]).update_one(**body, upsert=True)
            type_content = {"content": {}}
            body.update(type_content)
            return {
                "error": False,
                "message": "folder created", 
                "data": body
            }
        except Exception:
            return {
                "error": True,
                "message": "Internal Server Error"
            }

    @jwt_required
    def delete(self):
        try:
            body = request.get_json()
            folder = Folder.objects(id=body["id"]).delete()
            if folder:
                return {
                    "error": False,
                    "message": "Success delete folder"
                }
            else:
                return {
                    "error": True,
                    "message": "Folder does not exists"
                }
        except Exception:
            return {
                "error": True,
                "message": "Internal Server Error"
            }

class FolderFilesApi(Resource):
    @jwt_required
    def get(self, folder_id):
        try:
            folder = Document.objects(folder_id=folder_id).to_json()
            if folder:
                return {
                    "error": False,
                    "data": json.loads(folder)
                }
            else:
                return {
                    "error": True,
                    "message": "Folder does not exist"
                }
        except Exception:
            return {
                    "error": True,
                    "message": "Internal Server Error"
            }