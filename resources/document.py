from flask import request, Response
from flask_jwt_extended import jwt_required, decode_token
from flask_restful import Resource
from database.document import Document
import json

class DocumentsApi(Resource):
    @jwt_required
    def post(self):
        try:
            token = request.headers.get('Authorization').split()[1]
            identity = decode_token(token)
            body = request.get_json()
            if body['owner_id'] == 0:
                owner_id = identity['user_id']
            else:
                owner_id = str(body['owner_id'])
            company_id = identity['company_id']
            type_ = body['type']
            body.pop('type')
            body.update({
                'type_': type_,
                'owner_id': owner_id,
                'company_id': company_id
            })
            document = Document.objects(id=body["id"]).update_one(**body, upsert=True)
            return {
                "error": False,
                "message": "Document created", 
                "data": body
            }
        except Exception:
            return {
                "error": True,
                "message": "Internal Server Error"
            }

    @jwt_required
    def delete(self):
        id = request.get_json()['id']
        document = Document.objects(id=id).delete()
        if document:
            return {
                "error": False,
                "message": "Success delete document"
            }
        else:
            return {
                "error": True,
                "message": "Document does not exists"
            }
        
class DocumentApi(Resource):
    @jwt_required
    def get(self, document_id):
        documents = Document.objects(id=document_id).to_json()
        document = {}
        for d in json.loads(documents):
            document.update(d)
        if document:
            return {
                "error": False,
                "message": "Succes get document",
                "data": {
                    "document": document
                }
            }
        else:
            return {
                "error": True,
                "message": "Document does not exists"
            }