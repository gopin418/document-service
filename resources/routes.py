from .folder import FoldersApi, FolderApi, FolderFilesApi
from .document import DocumentsApi, DocumentApi

def initialize_routes(api):
    api.add_resource(FoldersApi, '/document-service')
    api.add_resource(FolderApi, '/document-service/folder')
    api.add_resource(FolderFilesApi, '/document-service/folder/<folder_id>')
    
    api.add_resource(DocumentsApi, '/document-service/document')
    api.add_resource(DocumentApi, '/document-service/document/<document_id>')