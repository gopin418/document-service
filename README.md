# Requirements 
* Required to run apps
    * MongoDb
    * Python 3
    * Pip
    
    
    
# Installation

#### Init pip virtual env

```
$ make init
```


#### Install dependencies

```
$ make install 
```


#### Start server

```
$ make start
```