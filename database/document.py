from .db import db

class Content(db.EmbeddedDocument):
    type = db.StringField()
    text = db.StringField()

class Blocks(db.EmbeddedDocument):
    blocks = db.ListField(db.EmbeddedDocumentField(Content))

class Document(db.Document):
    id = db.UUIDField(binary=False, primary_key=True)
    name = db.StringField()
    type_ = db.StringField()
    folder_id = db.UUIDField(binary=False)
    content = db.DictField(db.EmbeddedDocumentField(Blocks))
    owner_id = db.IntField()
    share = db.ListField()
    company_id = db.IntField()
    timestamp = db.IntField()