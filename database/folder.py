from .db import db

class Folder(db.Document):
    id = db.UUIDField(binary=False, primary_key=True)
    name = db.StringField()
    type_ = db.StringField(default="folder")
    is_public = db.BooleanField()
    owner_id = db.StringField()
    share = db.ListField()
    company_id = db.StringField()
    timestamp = db.IntField()