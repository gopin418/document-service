from flask import Flask
from dotenv import load_dotenv
from flask_jwt_extended import JWTManager
from database.db import initialize_db
from flask_restful import Api
from resources.routes import initialize_routes

load_dotenv()
import os

app = Flask(__name__)
app.config["JWT_SECRET_KEY"] = os.getenv("JWT_SECRET_KEY")
app.config["JWT_DECODE_AUDIENCE"] = os.getenv("JWT_DECODE_AUDIENCE")
app.config["JWT_IDENTITY_CLAIM"] = os.getenv("JWT_IDENTITY_CLAIM")

api = Api(app)
jwt = JWTManager(app)

app.config['MONGODB_SETTINGS'] = {
    'host': 'mongodb://localhost/document-service'
}

initialize_db(app)
initialize_routes(api)

app.run()